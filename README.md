# sidecar-xray

AWS XRay Sidecar

# Project Layout

```bash
.
├── 1.xx.x.x                    - sidecar version 1.xx.x.x
│   ├── docker-entrypoint.sh    - shell script run by docker container at startup
│   ├── Dockerfile              - Dockerfile to build the images for this project
│   └── service-envoy.yaml      - Envoy service configuration  
└── README.md                   - this file
├── .gitlab-ci.yml              - CI file that creats gitlab pipeline
├── .gitignore                  - Files to be ignored when commiting to git
├── variables.yml               - Project specific variables used in CICD pipeline jobs.
```

# Note: 
Refer to pipelines common repo - https://gitlab.healthcareit.net/edop/platform-services/sidecars/pipelines-common for CICD pipeline code.  

Turnkey assets project  - 
https://gitlab.healthcareit.net/imn-platform/platform-services/turnkey-assets  should be executed to sync the new image with all repos.
